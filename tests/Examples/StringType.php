<?php

declare(strict_types=1);

namespace Tests\Examples;

use DNC\Enum\Type\StringEnum;

class StringType extends StringEnum
{
    public const USER  = 'user';
    public const ADMIN = 'admin';

    /**
     * @return array
     */
    public static function getValues(): array
    {
        return [
            self::USER,
            self::ADMIN,
        ];
    }

    /**
     * @return static
     */
    public static function user(): self
    {
        return self::make(self::USER);
    }

    /**
     * @return static
     */
    public static function admin(): self
    {
        return self::make(self::ADMIN);
    }
}
