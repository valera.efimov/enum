<?php

declare(strict_types=1);

namespace DNC\Enum\Type;

use DNC\Enum\AbstractEnum;
use DNC\Enum\InvalidNameException;
use DNC\Enum\InvalidValueException;

abstract class IntegerEnum extends AbstractEnum
{
    /**
     * @var int
     */
    protected int $value;

    /**
     * @param int $value
     */
    final protected function __construct(int $value)
    {
        $this->validate($value);

        $this->value = $value;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    final public static function make(int $value)
    {
        $class = static::class;

        if (isset(self::$instances[$class][$value])) {
            return self::$instances[$class][$value];
        }

        $instance = new static($value);

        return self::$instances[$class][$value] = $instance;
    }

    /**
     * @param string $name
     *
     * @return static
     */
    final public static function makeFromName(string $name)
    {
        $class = static::class;

        if (!isset(self::$values[$class])) {
            self::$values[$class] = static::getValues();
        }

        if (!isset(self::$values[$class][$name])) {
            throw new InvalidNameException("Invalid name [{$name}] for [{$class}]");
        }

        return static::make(self::$values[$class][$name]);
    }

    /**
     * @return string[]
     */
    final public static function getNames(): array
    {
        return array_keys(static::getValues());
    }

    /**
     * @param int $value
     */
    final public function validate(int $value): void
    {
        $class = static::class;

        if (!isset(self::$values[$class])) {
            self::$values[$class] = static::getValues();
        }

        if (!in_array($value, self::$values[$class])) {
            throw new InvalidValueException("Invalid value [{$value}] for [{$class}]");
        }
    }

    /**
     * @return int
     */
    final public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @return string
     */
    final public function getName(): string
    {
        return array_search($this->value, self::$values[static::class], true);
    }
}
