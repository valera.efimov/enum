<?php

declare(strict_types=1);

namespace DNC\Enum\Type;

use DNC\Enum\AbstractEnum;
use DNC\Enum\InvalidValueException;

abstract class StringEnum extends AbstractEnum
{
    /**
     * @var string
     */
    protected string $value;

    /**
     * @param string $value
     */
    final protected function __construct(string $value)
    {
        $this->validate($value);

        $this->value = $value;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    final public static function make(string $value)
    {
        $class = static::class;

        if (isset(self::$instances[$class][$value])) {
            return self::$instances[$class][$value];
        }

        $instance = new static($value);

        return self::$instances[$class][$value] = $instance;
    }

    /**
     * @param string $value
     */
    final public function validate(string $value): void
    {
        $class = static::class;

        if (!isset(self::$values[$class])) {
            self::$values[$class] = static::getValues();
        }

        if (!in_array($value, self::$values[$class])) {
            throw new InvalidValueException("Invalid value [{$value}] for [{$class}]");
        }
    }

    /**
     * @return string
     */
    final public function getValue(): string
    {
        return $this->value;
    }
}
