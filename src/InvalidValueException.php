<?php

declare(strict_types=1);

namespace DNC\Enum;

use InvalidArgumentException;

class InvalidValueException extends InvalidArgumentException
{

}
